import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_app/utils/colors.dart';
import 'package:shopping_app/utils/dimensions.dart';
import 'package:shopping_app/widgets/app_text_field.dart';
import 'package:shopping_app/widgets/big_text.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var emailController = TextEditingController();
    var passwordController = TextEditingController();
    var phoneController = TextEditingController();
    var nameController = TextEditingController();

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SizedBox(
            height: Dimensions.screenHeight * 0.05,
          ),
          // app logo
          Container(
            height: Dimensions.screenHeight * 0.25,
            child: Center(
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 80,
                backgroundImage: AssetImage(
                  'assets/images/logo_flutter.png',
                ),
              ),
            ),
          ),
          // your email
          AppTextField(
              textController: emailController,
              hintText: 'Email',
              icon: Icons.email),
          SizedBox(
            height: Dimensions.height20,
          ),
          // your password
          AppTextField(
              textController: passwordController,
              hintText: 'Password',
              icon: Icons.password_sharp),
          SizedBox(
            height: Dimensions.height20,
          ),
          // your phone
          AppTextField(
              textController: phoneController,
              hintText: 'Phone',
              icon: Icons.phone_android),
          SizedBox(
            height: Dimensions.height20,
          ),
          // your name
          AppTextField(
              textController: nameController,
              hintText: 'Name',
              icon: Icons.person),
          SizedBox(
            height: Dimensions.height20 + Dimensions.height20,
          ),
          // Sign up button
          Container(
            width: Dimensions.screenWidth / 2,
            height: Dimensions.screenHeight / 13,
            decoration: BoxDecoration(
              color: AppColors.mainColor,
              borderRadius: BorderRadius.circular(Dimensions.radius30),
            ),
            child: Center(
              child: BigText(
                text: 'Sign up',
                size: Dimensions.font20 + Dimensions.font20 / 2,
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(
            height: Dimensions.height10,
          ),
          RichText(
            text: TextSpan(
              /**
               * TODO: recognizer same like gesture detector
               * but this is for text
               */
              recognizer: TapGestureRecognizer()..onTap = () => Get.back,
              text: 'Have an account already?',
              style: TextStyle(
                color: Colors.grey[500],
                fontSize: Dimensions.font26,
              ),
            ),
          ),
          
        ],
      ),
    );
  }
}

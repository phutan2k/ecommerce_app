import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shopping_app/base/no_data_page.dart';
import 'package:shopping_app/controllers/cart_controller.dart';
import 'package:shopping_app/routes/route_helper.dart';
import 'package:shopping_app/utils/app_constants.dart';
import 'package:shopping_app/utils/colors.dart';
import 'package:shopping_app/utils/dimensions.dart';
import 'package:shopping_app/widgets/app_icon.dart';
import 'package:shopping_app/widgets/big_text.dart';
import 'package:shopping_app/widgets/small_text.dart';

import '../../models/cart_model.dart';

class CartHistory extends StatelessWidget {
  const CartHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var getCartListHistory =
        Get.find<CartController>().getCartHistoryList().reversed.toList();
    // TODO: Giỏ hàng cho mỗi đơn hàng
    Map<String, int> cartItemsPerOrder = Map();

    for (int i = 0; i < getCartListHistory.length; i++) {
      if (cartItemsPerOrder.containsKey(getCartListHistory[i].time)) {
        // TODO: các lần tiếp theo tăng giá trị value lên 1 đơn vị. value là giá trị cũ
        cartItemsPerOrder.update(
            getCartListHistory[i].time!, (value) => ++value);
      } else {
        // TODO: lần đầu tiên truyền giá trị 1
        cartItemsPerOrder.putIfAbsent(getCartListHistory[i].time!, () => 1);
      }
    }

    print(cartItemsPerOrder);

    // số lần đặt hàng trong mỗi giỏ hàng
    List<int> cartItemsPerOrderToList() {
      // return cartItemsPerOrder.entries.map((e) {
      //   return e.value;
      // }).toList();
      return cartItemsPerOrder.entries.map((e) => e.value).toList();
    }

    List<String> cartOrderTimeToList() {
      return cartItemsPerOrder.entries.map((e) => e.key).toList();
    }

    List<int> itemsPerOrder = cartItemsPerOrderToList(); // 2, 5, 3
    print(itemsPerOrder);

    var listCounter = 0;

    Widget timeWidget(int index) {
      var outputDate = DateTime.now().toString();

      if(index < getCartListHistory.length) {
        DateTime parseDate = DateFormat('yyyy-MM-dd HH:mm:ss')
            .parse(getCartListHistory[listCounter].time!);
        var inputDate = DateTime.parse(parseDate.toString());
        var outputFormat = DateFormat('MM/dd/yyyy hh:mm a');
        outputDate = outputFormat.format(inputDate);
      }
      return BigText(text: outputDate);
    }

    return Scaffold(
      body: Column(
        children: [
          Container(
            height: Dimensions.height10 * 10,
            color: AppColors.mainColor,
            width: double.maxFinite,
            padding: EdgeInsets.only(
              top: Dimensions.height45,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                BigText(
                  text: 'Cart History',
                  color: Colors.white,
                ),
                AppIcon(
                  icon: Icons.shopping_cart_outlined,
                  iconColor: AppColors.mainColor,
                  backgroundColor: AppColors.yellowColor,
                )
              ],
            ),
          ),
          GetBuilder<CartController>(builder: (_cartController) {
            return _cartController.getCartHistoryList().length > 0
                ? Expanded(
                    child: Container(
                      margin: EdgeInsets.only(
                        top: Dimensions.height20,
                        left: Dimensions.width20,
                        right: Dimensions.width20,
                      ),
                      /**
                 * TODO: Khi sử dụng ListView sẽ có khoảng trống padding ở top
                 * TODO: dùng MediaQuery.removePadding để xóa khoảng trống đó đi
                 */
                      child: MediaQuery.removePadding(
                        removeTop: true,
                        context: context,
                        child: ListView(
                          children: [
                            for (int i = 0; i < itemsPerOrder.length; i++)
                              Container(
                                height: Dimensions.height30 * 4,
                                margin: EdgeInsets.only(
                                    bottom: Dimensions.height20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    timeWidget(listCounter),
                                    SizedBox(
                                      height: Dimensions.height10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Wrap(
                                          direction: Axis.horizontal,
                                          children: List.generate(
                                              itemsPerOrder[i], (index) {
                                            if (listCounter <
                                                getCartListHistory.length) {
                                              listCounter++;
                                            }
                                            return index <= 2
                                                ? Container(
                                                    width:
                                                        Dimensions.width20 * 4,
                                                    height:
                                                        Dimensions.height20 * 4,
                                                    margin: EdgeInsets.only(
                                                        right:
                                                            Dimensions.width10 /
                                                                2),
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius
                                                          .circular(Dimensions
                                                                  .radius15 /
                                                              2),
                                                      image: DecorationImage(
                                                        /**
                                             * TODO: Giải thích listCounter:
                                             * ta cần truyền index vào getCartListHistory để
                                             * lấy giá trị. Làm cách nào khi đang không có index.
                                             *
                                             * Giải pháp: tạo 1 biến listCounter = 0. Check:
                                             * Nếu listCounter < getCartListHistory.length
                                             * thì listCounter++ => 0, 1, 2, ...
                                             * => listCounter là index của getCartListHistory
                                             *
                                             * Tại sao lại là getCartListHistory[listCounter - 1]??
                                             * Vì lần đầu tiên listCounter = 0. Check điều kiện:
                                             * if (listCounter < getCartListHistory.length) {
                                             *   listCounter++; // listCounter = 1
                                             * }
                                             * getCartListHistory[listCounter] -> getCartListHistory[1]
                                             * => sai.
                                             * => getCartListHistory[listCounter - 1] ->
                                             * getCartListHistory[1 - 1] ->
                                             * getCartListHistory[0]
                                             */
                                                        image: NetworkImage(AppConstants
                                                                .BASE_URL +
                                                            AppConstants
                                                                .UPLOAD_URI +
                                                            getCartListHistory[
                                                                    listCounter -
                                                                        1]
                                                                .img!),
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  )
                                                : Container();
                                          }),
                                        ),
                                        Container(
                                          height: Dimensions.height20 * 4,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              SmallText(
                                                text: 'Total',
                                                color: AppColors.titleColor,
                                              ),
                                              BigText(
                                                text:
                                                    '${itemsPerOrder[i].toString()} Items',
                                                color: AppColors.titleColor,
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  /**
                                             * TODO: Làm sao để biết khi click vào 'one more' sẽ show ra
                                             * TODO: màn hình CartPage với các item của mỗi đơn hàng tương ứng??
                                             *
                                             * Giải pháp:
                                             * _ Vì mỗi đơn hàng đều có thời gian check-out
                                             * => Lấy được list thời gian order: orderTime = cartOrderTimeToList();
                                             *
                                             * orderTime có dạng:
                                             * [
                                             *  "2024-02-05 21:00:20.665010",
                                             *
                                             *  "2024-02-06 21:00:20.665010",
                                             *
                                             *  "2024-02-07 21:00:20.665010",
                                             * ]
                                             *
                                             * - Duyệt vòng lặp cho j chạy từ 0 đến < getCartListHistory
                                             *
                                             * getCartListHistory có dạng:
                                             * [
                                             *  {"id":9,"name":"Biriani1","time":"2024-02-05 21:00:20.665010"},
                                             *  {"id":9,"name":"Biriani2","time":"2024-02-05 21:00:20.665010"},
                                             *  {"id":9,"name":"Biriani2","time":"2024-02-05 21:00:20.665010"},
                                             *
                                             *  {"id":9,"name":"Biriani1","time":"2024-02-06 21:00:20.665010"},
                                             *
                                             *  {"id":9,"name":"Biriani2","time":"2024-02-07 21:00:20.665010"},
                                             *  {"id":9,"name":"Biriani2","time":"2024-02-07 21:00:20.665010"},
                                             *  {"id":9,"name":"Biriani4","time":"2024-02-07 21:00:20.665010"},
                                             * ]
                                             *
                                             * - Nếu getCartListHistory[j].time == orderTime[i]
                                             * thì ta thêm các item của đơn hàng đó vào Map mới bằng cách:
                                             * TODO: moreOrder.putIfAbsent(key, value);
                                             * trong đó:
                                             *  _ key: getCartListHistory[j].id!
                                             *  _ value: CartModel.fromJson(jsonDecode(jsonEncode(getCartListHistory[j])))
                                             *
                                             *  TODO: Giải thích cách hiểu bài toán!!
                                             *  _ Thay vì:
                                             *    + Nghĩ làm sao để khi click vào nút 'one more' của item bất kỳ
                                             *  trong danh sách các đơn hàng của màn hình CartHistory. Để show màn hình
                                             *  CartPage với những item tương ứng của đơn hàng đó.
                                             *
                                             *  _ Hãy nghĩ:
                                             *    + Ta sẽ so sánh các item trong list của màn hình CartPage là getCartListHistory
                                             *    (ds được lấy ra từ sharedPreference)
                                             *  và các đơn hàng trong danh sách của màn hình CartHistory là orderTime.
                                             *  So sánh thời gian 'check-out'. Nếu các item của CarPage có time trùng với
                                             *  các đơn hàng của CartHistory thì ta thêm các item trong ds của CarPage đó vào
                                             *  Map mới là moreOrder - moreOrder.putIfAbsent(key, value);
                                             *
                                             *  => Khi click vào button 'more-order' -> navigate đến màn hình CartPage
                                             *  và show Map moreOrder.
                                             */
                                                  var orderTime =
                                                      cartOrderTimeToList();
                                                  /*print('');
                                          print('-----------------');
                                          print('');
                                          print('OrderTime gồm: $orderTime');
                                          print('getCartListHistory gồm: ${Get.find<CartController>().getCartHistory()}');*/
                                                  Map<int, CartModel>
                                                      moreOrder = {};

                                                  for (int j = 0;
                                                      j <
                                                          getCartListHistory
                                                              .length;
                                                      j++) {
                                                    if (getCartListHistory[j]
                                                            .time ==
                                                        orderTime[i]) {
                                                      // print('getCartListHistory[j] is index: $j and orderTime[i] is index $i');
                                                      /**
                                                 * TODO: Mục đích muốn convert từ Object sang Json:
                                                 * TODO: 1. convert Object -> String qua jsonEncode
                                                 * TODO: 2. convert String -> Json qua jsonDecode
                                                 */
                                                      moreOrder.putIfAbsent(
                                                          getCartListHistory[j]
                                                              .id!,
                                                          () => CartModel.fromJson(
                                                              jsonDecode(jsonEncode(
                                                                  getCartListHistory[
                                                                      j]))));
                                                      // print('More Order is: ${jsonEncode(getCartListHistory[j])}');
                                                    }
                                                  }
                                                  Get.find<CartController>()
                                                      .setItems = moreOrder;
                                                  Get.find<CartController>()
                                                      .addToCartList();
                                                  Get.toNamed(RouteHelper
                                                      .getCartPage());
                                                },
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                          Dimensions.width10,
                                                      vertical:
                                                          Dimensions.height10 /
                                                              2),
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius
                                                          .circular(Dimensions
                                                                  .radius15 /
                                                              3),
                                                      border: Border.all(
                                                          width: 1,
                                                          color: AppColors
                                                              .mainColor)),
                                                  child: SmallText(
                                                    text: 'one more',
                                                    color: AppColors.mainColor,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  )
                : SizedBox(
                    height: Dimensions.emptyBoxImg,
                    child: Center(
                      child: NoDataPage(
                        text: "You didn't buy anything so far!!",
                        imgPath: 'assets/images/ic_empty_box.png',
                      ),
                    ));
          }),
        ],
      ),
    );
  }
}

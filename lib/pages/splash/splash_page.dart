import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_app/routes/route_helper.dart';
import 'package:shopping_app/utils/colors.dart';
import 'package:shopping_app/utils/dimensions.dart';
import 'package:shopping_app/widgets/big_text.dart';

import '../../controllers/popular_product_controller.dart';
import '../../controllers/recommended_product_controller.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  Future<void> _loadResource() async {
    // TODO: Bắt đầu tải dữ liệu từ server về khi ở màn hình chờ: Splash screen
    await Get.find<PopularProductController>().getPopularProductList();
    await Get.find<RecommendedProductController>().getRecommendedProductList();
  }

  @override
  void initState() {
    super.initState();
    _loadResource();

    /**
     * AnyClass() {
     *
     *    newObject() {
     *        return ...;
     *    }
     * }
     *
     * var x = AnyClass()..newObject();
     *
     * <=>
     *
     * var x = AnyClass();
     * x = x.newObject();
     *
     * => it's just creating an instance first and from that instance
     * doing something different based on that instance
     */

    // TODO: Toán tử .. forward() ~ controller = controller.forward();
    // TODO: Cách viết như này: controller = controller.forward();
    // sẽ cần nhiều thuộc tính hơn

    // forward: Starts running this animation forwards
    // vsync: nhận vào current context
    controller =
        AnimationController(vsync: this, duration: Duration(seconds: 2))
          ..forward();

    // CurvedAnimation: điều chỉnh tốc độ của animation.
    // Curves.linear: Animation diễn ra với tốc độ đều
    animation = CurvedAnimation(parent: controller, curve: Curves.linear);

    Timer(
      Duration(seconds: 3),
      () => Get.offNamed(RouteHelper.getInitial()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // ScaleTransition: tạo hiệu ứng chuyển động
          ScaleTransition(
            scale: animation,
            child: Center(
                child: Image.asset(
              'assets/images/logo_food.png',
              // width: Dimensions.splashImg,
            )),
          ),
          SizedBox(
            height: Dimensions.height20,
          ),
          BigText(
            text: 'The Best Food',
            size: Dimensions.font26 * 1.5,
            color: AppColors.mainColor,
          ),
        ],
      ),
    );
  }
}

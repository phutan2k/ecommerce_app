import 'package:flutter/material.dart';
import 'package:shopping_app/utils/colors.dart';
import 'package:shopping_app/utils/dimensions.dart';

class AppTextField extends StatelessWidget {
  final TextEditingController textController;
  final String hintText;
  final IconData icon;

  const AppTextField(
      {Key? key,
      required this.textController,
      required this.hintText,
      required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.only(left: Dimensions.width20, right: Dimensions.width20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Dimensions.radius30),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            // bán kính của hiệu ứng mờ cho bóng đổ
            blurRadius: 10,
            /**
             * Bán kính của hiệu ứng lan :
             * _ Giá trị dương tạo ra bóng đổ mở rộng
             * _ Giá trị âm tạo ra bóng đổ thu hẹp
             */
            spreadRadius: 7,
            /**
             * Độ lệch của bóng đổ theo trục x và y
             */
            offset: Offset(1, 10),
            /**
             * TODO: withOpacity - tạo độ mờ mới của màu
             * Colors.grey.withOpacity(0.2) - tạo màu xám mờ đi 20%
             */
            color: Colors.grey.withOpacity(0.2),
          ),
        ],
      ),
      child: TextField(
        controller: textController,
        decoration: InputDecoration(
          // TODO: hintText
          hintText: hintText,
          // TODO: prefixIcon
          prefixIcon: Icon(
            icon,
            color: AppColors.mainColor,
          ),
          // TODO: focusedBorder
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(Dimensions.radius30),
            borderSide: BorderSide(
              width: 1.0,
              color: Colors.white,
            ),
          ),
          // TODO: enabledBorder
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(Dimensions.radius30),
            borderSide: BorderSide(
              width: 1.0,
              color: Colors.white,
            ),
          ),
          // TODO: border
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(Dimensions.radius30),
          ),
        ),
      ),
    );
  }
}

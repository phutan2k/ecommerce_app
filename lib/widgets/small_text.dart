import 'package:flutter/material.dart';

class SmallText extends StatelessWidget {
  Color? color;
  final String text;
  double size;
  double height;
  bool checkOverFlow;

  SmallText({
    Key? key,
    this.color = const Color(0xFFccc7c5),
    required this.text,
    this.size = 12,
    this.height = 1.2,
    this.checkOverFlow = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (checkOverFlow) {
      return Text(
        text,
        style: TextStyle(
          fontFamily: 'Roboto',
          color: color,
          fontSize: size,
          height: height,
          overflow: TextOverflow.ellipsis,
        ),
        maxLines: 2,
      );
    } else {
      return Text(
        text,
        style: TextStyle(
          fontFamily: 'Roboto',
          color: color,
          fontSize: size,
          height: height,
        ),
      );
    }
  }
}

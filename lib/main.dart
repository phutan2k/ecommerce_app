import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_app/controllers/cart_controller.dart';
import 'package:shopping_app/controllers/popular_product_controller.dart';
import 'package:shopping_app/controllers/recommended_product_controller.dart';
import 'package:shopping_app/pages/auth/sign_up_page.dart';
import 'package:shopping_app/routes/route_helper.dart';
import 'package:shopping_app/widgets/app_text_field.dart';

import 'helper/dependencies.dart' as dep;

void main() async {
  // nó sẽ khởi tạo để đảm bảo rằng các phần phụ thuộc của bạn được tải
  // chính xác và đợi cho đến khi chúng được tải
  WidgetsFlutterBinding.ensureInitialized();
  await dep.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    // TODO: restore data from sharedPreferences
    Get.find<CartController>().getCartData();
    /**
     * TODO: keep controllers in memories
     * GetBuilder: quản lý state và controller trong một widget.
     * GetBuilder phải được gọi trước GetMaterialApp.
     */
    return GetBuilder<PopularProductController>(builder: (_) {
      return GetBuilder<RecommendedProductController>(builder: (_) {
        return GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',

          home: SignUpPage(),
          // initialRoute: RouteHelper.getSplashPage(),
          // getPages: RouteHelper.routes,
        );
      });
    });
  }
}

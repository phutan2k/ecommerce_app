import 'package:get/get.dart';

class Dimensions {
  // Tương đương với: MediaQuery.of(context).size.height;
  static double screenHeight = Get.context!.height;

  // Tương đương với: MediaQuery.of(context).size.width;
  static double screenWidth = Get.context!.width;

  static double pageView = screenHeight / 2.64; // - 320
  static double pageViewContainer = screenHeight / 3.84; //3.6 - 220
  static double pageViewTextContainer = screenHeight / 6.5; //6.5 - 120

  // dynamic height padding and margin
  static double height10 = screenHeight / 84.4;
  static double height15 = screenHeight / 56.27;
  static double height20 = screenHeight / 42.2;
  static double height30 = screenHeight / 28.13;
  static double height45 = screenHeight / 18.76;

  // dynamic width padding and margin
  static double width10 = screenHeight / 84.4;
  static double width15 = screenHeight / 56.27;
  static double width20 = screenHeight / 42.2;
  static double width30 = screenHeight / 28.13;
  static double width45 = screenHeight / 18.76;

  // font size
  static double font16 = screenHeight / 52.75;
  static double font20 = screenHeight / 42.2;
  static double font26 = screenHeight / 32.46;

  // radius
  static double radius5 = screenHeight / 168.8;
  static double radius15 = screenHeight / 56.27;
  static double radius20 = screenHeight / 42.2;
  static double radius30 = screenHeight / 28.13;

  // icon size
  static double iconSize24 = screenHeight / 35.17;
  static double iconSize16 = screenHeight / 52.75;

  // list view size
  static double listViewImgSize = screenWidth / 3.25; // 120dp
  static double listViewTextContainerSize = screenWidth / 3.9; // 100dp

  // popular food
  static double popularFoodImgSize = screenHeight / 2.41;

  // bottom height
  static double bottomHeightBar = screenHeight / 7.03;

  // splash screen dimensions
  static double splashImg = screenHeight / 3.38;

  // empty box Cart History dimensions
  static double emptyBoxImg = screenHeight / 1.5;
}

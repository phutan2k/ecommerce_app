class AppConstants {
  static const String APP_NAME = 'DBFood';
  static const int APP_VERSION = 1;

  static const String BASE_URL = 'http://mvs.bslmeiyu.com';
  // static const String BASE_URL = 'http://10.0.2.2:8000';
  static const String POPULAR_PRODUCT_URI = '/api/v1/products/popular';
  static const String RECOMMENDED_PRODUCT_URI = '/api/v1/products/recommended';

  // sử dụng để thêm vào đường dẫn image
  static const String UPLOAD_URI = '/uploads/';

  static const String TOKEN = 'DBtoken';
  static const String CART_LIST = 'cart-list';
  static const String CART_HISTORY_LIST = 'cart-history-list';
}
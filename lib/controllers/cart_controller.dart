import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_app/data/repository/cart_repo.dart';
import 'package:shopping_app/models/products_model.dart';

import '../models/cart_model.dart';
import '../utils/colors.dart';

class CartController extends GetxController {
  final CartRepo cartRepo;

  CartController({required this.cartRepo});

  Map<int, CartModel> _items = {};

  Map<int, CartModel> get items => _items;

  // TODO: Only for storage and sharedPreferences
  List<CartModel> storageItems = [];


  void addItem(ProductModel product, int quantity) {
    var totalQuantity = 0;

    // TODO: containsKey(key) là phương thức của Map sử dụng để kiểm tra
    // TODO: key đó có tồn tại hay không
    if (_items.containsKey(product.id!)) {
      // TODO: Nếu key đó đã tồn tại trong Map thì sử dụng phương thức
      // TODO: update(key, (value) => ...) để cập nhật giá trị của Map
      // TODO: Ở đây value là Object
      _items.update(product.id!, (value) {
        totalQuantity = value.quantity! + quantity;

        return CartModel(
          id: value.id,
          name: value.name,
          price: value.price,
          img: value.img,
          quantity: value.quantity! + quantity,
          isExist: true,
          time: DateTime.now().toString(),
          product: product,
        );
      });

      // TODO: Sử dụng totalQuantity trong trường hợp:
      // Khi ta trừ quantity về 0 bằng việc click button -
      // sau đó click button Add to cart thì hệ thống sẽ trừ hết
      // số quantity bằng việc gọi _items.remove(product.id);
      // Nếu không sử dụng totalQuantity để check điều kiện thì
      // sẽ không được add vào storage vì quantity đang = 0
      if (totalQuantity <= 0) {
        _items.remove(product.id);
      }
    } else {
      if (quantity > 0) {
        // TODO: putIfAbsent(key, () => value) là 1 phương thức của Map
        // TODO: sử dụng để thêm 1 cặp key - value vào Map chỉ khi
        // TODO: key đó chưa tồn tại trong Map
        _items.putIfAbsent(product.id!, () {
          return CartModel(
            id: product.id,
            name: product.name,
            price: product.price,
            img: product.img,
            quantity: quantity,
            isExist: true,
            time: DateTime.now().toString(),
            product: product,
          );
        });
      } else {
        Get.snackbar('Warning', 'You should at least add an item in the cart',
            backgroundColor: AppColors.mainColor, colorText: Colors.white);
      }
    }
    // TODO: Save to SharedPreference
    cartRepo.addToCartList(getItems);
    update();
  }

  bool existInCart(ProductModel product) {
    if (_items.containsKey(product.id)) {
      return true;
    }
    return false;
  }

  int getQuantity(ProductModel product) {
    var quantity = 0;
    if (_items.containsKey(product.id)) {
      _items.forEach((key, value) {
        if (key == product.id) {
          quantity = value.quantity!;
        }
      });
    }
    return quantity;
  }

  int get totalItems {
    var totalQuantity = 0;
    _items.forEach((key, value) {
      totalQuantity += value.quantity!;
    });
    return totalQuantity;
  }

  // Return List CartModel từ Map
  List<CartModel> get getItems {
    return _items.entries.map((e) {
      return e.value;
    }).toList();
  }

  int get totalAmount {
    var total = 0;

    _items.forEach((key, value) {
      total += value.quantity! * value.price!;
    });
    return total;
  }

  // TODO: Getter
  List<CartModel> getCartData() {
    setCart = cartRepo.getCartList();
    return storageItems;
  }

  /**
   * TODO: Setter
   * Method này chỉ được gọi lần đầu tiên khi khởi động ứng dụng hoặc khi người
   * dùng tắt ứng dụng đi rồi khởi động lại.
   */
  set setCart(List<CartModel> items) {
    storageItems = items;
    print('Length of cart items: ${storageItems.length.toString()}');

    /**
     * TODO: update danh sách items
     * Khi khởi chạy ứng dụng lần đầu tiên, Map items sẽ bị trống.
     * Do đó ta cần put data từ storage vào Map _items
     */
    for(int i = 0; i < storageItems.length; i++) {
      _items.putIfAbsent(storageItems[i].product!.id!, () => storageItems[i]);
    }
  }

  void addToHistory() {
    cartRepo.addToCartHistoryList();
    clear();
  }

  void clear() {
    _items = {};
    update();
  }

  List<CartModel> getCartHistoryList() {
    return cartRepo.getCartHistoryList();
  }

  // TODO: Debug
  List<String> getCartHistory() => cartRepo.getCartHistory();

  set setItems(Map<int, CartModel> setItems) {
    _items = {};
    _items = setItems;
  }

  void addToCartList() {
    // TODO: Save to SharedPreference
    cartRepo.addToCartList(getItems);
    update();
  }
}
